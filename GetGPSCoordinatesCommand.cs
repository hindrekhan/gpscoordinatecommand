using ExifLibrary;
using MediaGallery.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using MediaGallery.Data;
using MediaGallery.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.ProjectOxford.Face;
using Microsoft.ProjectOxford.Face.Contract;
using Microsoft.ProjectOxford.Vision;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using Microsoft.CodeAnalysis.Diagnostics;
using System.Diagnostics.CodeAnalysis;

namespace MediaGallery.Commands
{
    public class GetGPSCoordinatesCommand : ICommand<PhotoEditModel>
    {
        private readonly ILogger<GetGPSCoordinatesCommand> _logger;
        public GetGPSCoordinatesCommand(ILogger<GetGPSCoordinatesCommand> logger)
        {
            _logger = logger;
        }
        public async Task<bool> Execute(PhotoEditModel model)
        {
            if (model == null)
            {
                _logger.LogWarning("GetGPSCoordinatesCommand Model is Null");
                return true;                
            }

            if(model.File== null)
            {
                _logger.LogWarning("GetGPSCoordinatesCommand Model.File is Null");
                return true;
            }

            var imageInfo = await GetImageInfo(model);
            if (imageInfo == null)
            {
                _logger.LogWarning("GetGPSCoordinatesCommand ImageInfo is Null");
                return true;
            }

            var latitude = imageInfo.Item1;
            var longitude = imageInfo.Item2;

            if (latitude != null && longitude != null)
            {
                model.Latitude = latitude.ToFloat();
                model.Longitude = longitude.ToFloat();

                return true;
            }

            _logger.LogWarning("GetGPSCoordinatesCommand latitude or longitude is Null");
            return true;
        }

        internal virtual async Task<Tuple<GPSLatitudeLongitude, GPSLatitudeLongitude>> GetImageInfo(PhotoEditModel model)
        {
            var img = ImageFile.FromStream(model.File.OpenReadStream());

            var latidude = (GPSLatitudeLongitude)img.Properties.FirstOrDefault(p => p.Name == "GPSLatitude");
            var longitude = (GPSLatitudeLongitude)img.Properties.FirstOrDefault(p => p.Name == "GPSLongitude");

            return Tuple.Create(latidude, longitude);
        }

        [ExcludeFromCodeCoverage]
        public bool Rollback()
        {
            return true;
        }

        [ExcludeFromCodeCoverage]
        public async Task<List<string>> Validate(PhotoEditModel parameter)
        {
            return new List<string>();
        }
    }
}